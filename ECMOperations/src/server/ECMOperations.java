/**
 * 
 */
package server;

import java.util.logging.Logger;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Connection;
import com.filenet.api.core.ContentReference;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.EntireNetwork;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.Properties;

import filenet.vw.api.VWAttachment;
import filenet.vw.api.VWAttachmentType;
import filenet.vw.api.VWException;
import filenet.vw.api.VWLibraryType;
import filenet.vw.api.VWSession;
import filenet.vw.server.Configuration;
import filenet.vw.server.VWLoginModule;

/**
 * @author Ricardo Belfor
 *
 */
public class ECMOperations {

	private static final String CLASS_NAME = "ECMOperations";
	private static final String METHOD_NAME = "createExternalDocument";
	
	private static Logger logger = Logger.getLogger( ECMOperations.class.getName() );
	
	public VWAttachment createExternalDocument(String className, String[] propertyNames, String[] propertyValues, String reference, String mimeType) throws Exception {

		logger.entering(CLASS_NAME, METHOD_NAME);
		
		try {
			Document document = createDocument( getObjectStore(), className, propertyNames, propertyValues, reference, mimeType);
			VWAttachment attachment = getAsVWAttachment(document);
			logger.exiting(CLASS_NAME, METHOD_NAME, attachment.toString() );
			return attachment;
		} catch (Exception e) {
			logger.throwing(CLASS_NAME, METHOD_NAME, e);
			throw e;
		}
	}

	private ObjectStore getObjectStore() throws Exception {
		EntireNetwork entireNetwork = Factory.EntireNetwork.fetchInstance(getConnection(), null);
		Domain domain = entireNetwork.get_LocalDomain();
		return Factory.ObjectStore.fetchInstance(domain,  getObjectStoreName(), null);
	}

	private String getObjectStoreName() throws Exception {
		VWSession vwSession = VWLoginModule.getSession();
	    return vwSession.getObjectStoreSymbolicName();
	}

	private Connection getConnection() {
		String ceURI = Configuration.GetCEURI(null, null);
		return Factory.Connection.getConnection(ceURI);
	}

	private Document createDocument(ObjectStore objectStore, String className, String[] propertyNames, String propertyValues[], String reference, String mimeType) {

		Document doc = Factory.Document.createInstance(objectStore, className);
		Properties properties = doc.getProperties();
		for (int i = 0; i < propertyNames.length; ++i ) {
			properties.putObjectValue(propertyNames[i], propertyValues[i]);
		}
		doc.set_ContentElements( getContentElements(reference, mimeType) );
		doc.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION );
		doc.save(RefreshMode.REFRESH);
		
		return doc;
	}	

	@SuppressWarnings("unchecked")
	private ContentElementList getContentElements(String reference, String mimeType) {
		ContentElementList contentElementList = Factory.ContentElement.createList();
		contentElementList.add( createExternalContent(reference,mimeType) );
		return contentElementList;
	}

	private Object createExternalContent(String reference, String mimeType) {
		ContentReference contentReference = Factory.ContentReference.createInstance();
		contentReference.set_ContentLocation(reference);
		contentReference.set_ContentType(mimeType);
		return contentReference;
	}	

	private VWAttachment getAsVWAttachment(Document document) throws VWException {

		VWAttachment documentAttachment = new VWAttachment();

		documentAttachment.setLibraryType( VWLibraryType.LIBRARY_TYPE_CONTENT_ENGINE );
		ObjectStore objectStore = document.getObjectStore();
		objectStore.fetchProperties( new String[] { PropertyNames.NAME } );

		documentAttachment.setLibraryName( objectStore.get_Name() );
		documentAttachment.setId( document.get_Id().toString() );
		documentAttachment.setAttachmentName( document.get_Name() );
		documentAttachment.setType( VWAttachmentType.ATTACHMENT_TYPE_FOLDER );

		return documentAttachment;
	}
}
